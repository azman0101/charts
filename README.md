# Charts-github

This repository has 2 main purposes:

1. version helm chart source code, in src folder of master branch
2. act as helm repository, in charts folder


## Updating a chart

0. pull master changes
1. merge origin/auto-reindex into master
2. patch the chart and commit changes to your branch
3. merge your branch to master
4. publish th new version of the chart
5. check that your new version is published


### For example, to update robots-txt chart

#### 0. Pull `master` changes

```
git checkout master
git pull
```

#### 1. Merge `origin/auto-reindex` into master.

This step is needed to avoid losing commits done during pipeline. Check `.gitlab-ci.yml` for more details.

```
git merge --ff orign/auto-reindex
```

#### 2. DO YOUR STUFF

Update a chart, fix a bug, do your stuff. *In a new branch.*

Do not forget to increase Chart version in file `Chart.yaml` !

```
git checkout -b feature/improve-value-files-readability
vim Chart.yaml   # bump chart version
...
git add -p
git commit
```

#### 3. Merge your branch into master

```
git checkout master
git pull
git merge --no-ff feature/improve-value-files-readability
```

#### 4. Publish new version of the chart

```
git push origin master
```

#### 5. Check that new version is available in repository

Open the following link:

https://rsicart.gitlab.io/charts/index.yaml
